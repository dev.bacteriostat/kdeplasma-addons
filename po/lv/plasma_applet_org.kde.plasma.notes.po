# translation of plasma_applet_notes.po to Latvian
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Maris Nartiss <maris.kde@gmail.com>, 2007.
# Viesturs Zarins <viesturs.zarins@mii.lu.lv>, 2008.
# Viesturs Zariņš <viesturs.zarins@mii.lu.lv>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_notes\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-10-06 01:41+0000\n"
"PO-Revision-Date: 2009-06-03 20:40+0300\n"
"Last-Translator: Viesturs Zariņš <viesturs.zarins@mii.lu.lv>\n"
"Language-Team: Latvian <locale@laka.lv>\n"
"Language: lv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : "
"2);\n"

#: package/contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "Appearance"
msgstr ""

#: package/contents/ui/configAppearance.qml:32
#, kde-format
msgid "%1pt"
msgstr ""

#: package/contents/ui/configAppearance.qml:38
#, fuzzy, kde-format
#| msgid "Use custom font size:"
msgid "Text font size:"
msgstr "Lietot pielāgotu fonta izmēru:"

#: package/contents/ui/configAppearance.qml:43
#, kde-format
msgid "Background color"
msgstr ""

#: package/contents/ui/configAppearance.qml:74
#, kde-format
msgid "A white sticky note"
msgstr ""

#: package/contents/ui/configAppearance.qml:75
#, kde-format
msgid "A black sticky note"
msgstr ""

#: package/contents/ui/configAppearance.qml:76
#, kde-format
msgid "A red sticky note"
msgstr ""

#: package/contents/ui/configAppearance.qml:77
#, kde-format
msgid "An orange sticky note"
msgstr ""

#: package/contents/ui/configAppearance.qml:78
#, kde-format
msgid "A yellow sticky note"
msgstr ""

#: package/contents/ui/configAppearance.qml:79
#, kde-format
msgid "A green sticky note"
msgstr ""

#: package/contents/ui/configAppearance.qml:80
#, kde-format
msgid "A blue sticky note"
msgstr ""

#: package/contents/ui/configAppearance.qml:81
#, kde-format
msgid "A pink sticky note"
msgstr ""

#: package/contents/ui/configAppearance.qml:82
#, kde-format
msgid "A translucent sticky note"
msgstr ""

#: package/contents/ui/configAppearance.qml:83
#, kde-format
msgid "A translucent sticky note with light text"
msgstr ""

#: package/contents/ui/main.qml:267
#, kde-format
msgid "Undo"
msgstr ""

#: package/contents/ui/main.qml:275
#, kde-format
msgid "Redo"
msgstr ""

#: package/contents/ui/main.qml:285
#, kde-format
msgid "Cut"
msgstr ""

#: package/contents/ui/main.qml:293
#, kde-format
msgid "Copy"
msgstr ""

#: package/contents/ui/main.qml:301
#, kde-format
msgid "Paste"
msgstr ""

#: package/contents/ui/main.qml:307
#, kde-format
msgid "Paste with Full Formatting"
msgstr ""

#: package/contents/ui/main.qml:314
#, fuzzy, kde-format
#| msgid "Formatting"
msgctxt "@action:inmenu"
msgid "Remove Formatting"
msgstr "Formāts"

#: package/contents/ui/main.qml:329
#, kde-format
msgid "Delete"
msgstr ""

#: package/contents/ui/main.qml:336
#, kde-format
msgid "Clear"
msgstr ""

#: package/contents/ui/main.qml:346
#, kde-format
msgid "Select All"
msgstr ""

#: package/contents/ui/main.qml:474
#, fuzzy, kde-format
#| msgid "Bold"
msgctxt "@info:tooltip"
msgid "Bold"
msgstr "Trekns"

#: package/contents/ui/main.qml:486
#, fuzzy, kde-format
#| msgid "Italic"
msgctxt "@info:tooltip"
msgid "Italic"
msgstr "Slīpraksts"

#: package/contents/ui/main.qml:498
#, fuzzy, kde-format
#| msgid "Underline"
msgctxt "@info:tooltip"
msgid "Underline"
msgstr "Pasvītrots"

#: package/contents/ui/main.qml:510
#, fuzzy, kde-format
#| msgid "StrikeOut"
msgctxt "@info:tooltip"
msgid "Strikethrough"
msgstr "Pārsvīrtrots"

#: package/contents/ui/main.qml:584
#, kde-format
msgid "Discard this note?"
msgstr ""

#: package/contents/ui/main.qml:585
#, kde-format
msgid "Are you sure you want to discard this note?"
msgstr ""

#: package/contents/ui/main.qml:605
#, fuzzy, kde-format
#| msgid "White"
msgctxt "@item:inmenu"
msgid "White"
msgstr "Balts"

#: package/contents/ui/main.qml:609
#, fuzzy, kde-format
#| msgid "Black"
msgctxt "@item:inmenu"
msgid "Black"
msgstr "Melns"

#: package/contents/ui/main.qml:613
#, fuzzy, kde-format
#| msgid "Red"
msgctxt "@item:inmenu"
msgid "Red"
msgstr "Sarkans"

#: package/contents/ui/main.qml:617
#, fuzzy, kde-format
#| msgid "Orange"
msgctxt "@item:inmenu"
msgid "Orange"
msgstr "Oranžs"

#: package/contents/ui/main.qml:621
#, fuzzy, kde-format
#| msgid "Yellow"
msgctxt "@item:inmenu"
msgid "Yellow"
msgstr "Dzeltens"

#: package/contents/ui/main.qml:625
#, fuzzy, kde-format
#| msgid "Green"
msgctxt "@item:inmenu"
msgid "Green"
msgstr "Zaļš"

#: package/contents/ui/main.qml:629
#, fuzzy, kde-format
#| msgid "Blue"
msgctxt "@item:inmenu"
msgid "Blue"
msgstr "Zils"

#: package/contents/ui/main.qml:633
#, fuzzy, kde-format
#| msgid "Pink"
msgctxt "@item:inmenu"
msgid "Pink"
msgstr "Rozā"

#: package/contents/ui/main.qml:637
#, fuzzy, kde-format
#| msgid "Translucent"
msgctxt "@item:inmenu"
msgid "Translucent"
msgstr " Caurspīdīga"

#: package/contents/ui/main.qml:641
#, fuzzy, kde-format
#| msgid "Translucent"
msgctxt "@item:inmenu"
msgid "Translucent Light"
msgstr " Caurspīdīga"

#, fuzzy
#~| msgid "Justify center"
#~ msgid "Align center"
#~ msgstr "Izlīdzināt centrā"

#, fuzzy
#~| msgid "Justify"
#~ msgid "Justified"
#~ msgstr "Izlīdzināt"

#~ msgid "Font"
#~ msgstr "Fonts"

#~ msgid "Style:"
#~ msgstr "Stils:"

#~ msgid "&Bold"
#~ msgstr "&Trekns"

#~ msgid "&Italic"
#~ msgstr "&Slīpraksts"

#~ msgid "Size:"
#~ msgstr "Izmērs:"

#~ msgid "Scale font size by:"
#~ msgstr "Mērogot fonta izmēru par:"

#~ msgid "%"
#~ msgstr "%"

#~ msgid "Color:"
#~ msgstr "Krāsa:"

#~ msgid "Use theme color"
#~ msgstr "Lietot tēmas krāsu"

#~ msgid "Use custom color:"
#~ msgstr "Lietot pielāgotu krāsu:"

#~ msgid "Active line highlight color:"
#~ msgstr "Aktīvās rindas izcelšanas krāsa:"

#~ msgid "Use no color"
#~ msgstr "Nav krāsas"

#~ msgid "Theme"
#~ msgstr "Tēma"

#~ msgid "Notes color:"
#~ msgstr "Lapiņas krāsa:"

#~ msgid "Spell Check"
#~ msgstr "Pareizrakstība"

#~ msgid "Enable spell check:"
#~ msgstr "Ieslēgt pareizrakstības pārbaudi:"

#~ msgid "Notes Color"
#~ msgstr "Lapiņas krāsa"

#~ msgid "General"
#~ msgstr "Pamata"

#~ msgid "Unable to open file"
#~ msgstr "Neizdevās atvērt failu"

#~ msgid "Welcome to the Notes Plasmoid! Type your notes here..."
#~ msgstr "Laipni lūgti piezīmju sīkrīkā! Rakstiet šeit savas piezīmes..."

#, fuzzy
#~| msgid "Color:"
#~ msgid "Text Color"
#~ msgstr "Krāsa:"

#~ msgid "Check spelling"
#~ msgstr "Pārbaudīt pareizrakstību"
