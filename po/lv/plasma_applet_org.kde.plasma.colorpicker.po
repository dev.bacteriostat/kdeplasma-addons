# translation of plasma_applet_kolourpicker.po to Latvian
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Maris Nartiss <maris.kde@gmail.com>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_kolourpicker\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-11 01:44+0000\n"
"PO-Revision-Date: 2008-01-03 21:34+0200\n"
"Last-Translator: Maris Nartiss <maris.kde@gmail.com>\n"
"Language-Team: Latvian <locale@laka.lv>\n"
"Language: lv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : "
"2);\n"

#: package/contents/config/config.qml:11
#, kde-format
msgctxt "@title"
msgid "General"
msgstr ""

#: package/contents/ui/ColorCircle.qml:41
#, kde-format
msgctxt "@info:usagetip"
msgid "Middle-click to copy the color code"
msgstr ""

#: package/contents/ui/CompactRepresentation.qml:74
#, kde-format
msgctxt "@info:tooltip"
msgid "Pick color"
msgstr ""

#: package/contents/ui/CompactRepresentation.qml:81
#, kde-kuit-format
msgctxt "@info:usagetip"
msgid ""
"Drag a color code here to save it<nl/>Drag an image file here to get its "
"average color"
msgstr ""

#: package/contents/ui/configGeneral.qml:25
#, kde-format
msgctxt "@label:listbox"
msgid "Default color format:"
msgstr ""

#: package/contents/ui/configGeneral.qml:33
#, kde-format
msgctxt "@option:check"
msgid "Automatically copy color to clipboard"
msgstr ""

#: package/contents/ui/configGeneral.qml:41
#, kde-format
msgctxt "@label"
msgid "When pressing the keyboard shortcut:"
msgstr ""

#: package/contents/ui/configGeneral.qml:42
#, kde-format
msgctxt "@option:radio"
msgid "Pick a color"
msgstr ""

#: package/contents/ui/configGeneral.qml:48
#, fuzzy, kde-format
#| msgid "History"
msgctxt "@option:radio"
msgid "Show history"
msgstr "Vēsture"

#: package/contents/ui/configGeneral.qml:58
#, kde-format
msgctxt "@label"
msgid "Preview count:"
msgstr ""

#: package/contents/ui/main.qml:101
#, fuzzy, kde-format
#| msgid "Copy Color Value"
msgctxt "@action"
msgid "Open Color Dialog"
msgstr "Kopēt krāsas vērtību"

#: package/contents/ui/main.qml:106
#, fuzzy, kde-format
#| msgid "Clear History"
msgctxt "@action"
msgid "Clear History"
msgstr "Tīrīt vēsturi"

#: package/contents/ui/main.qml:114
#, fuzzy, kde-format
#| msgid "Clear History"
msgctxt "@action"
msgid "View History"
msgstr "Tīrīt vēsturi"

#: package/contents/ui/main.qml:122
#, kde-format
msgctxt "@title:menu"
msgid "Copy to Clipboard"
msgstr ""

#: package/contents/ui/main.qml:162
#, kde-format
msgctxt "@info:usagetip"
msgid "No colors"
msgstr ""

#: package/contents/ui/main.qml:162
#, kde-format
msgctxt "@info:status when color picking is unavailable"
msgid "Color picking unavailable when compositing is disabled"
msgstr ""

#: package/contents/ui/main.qml:163
#, kde-format
msgctxt "@info:status when color pick is unavailable"
msgid ""
"Compositing has been manually disabled or blocked by a running application"
msgstr ""

#: package/contents/ui/main.qml:170
#, kde-format
msgctxt "@action:button"
msgid "Pick Color"
msgstr ""

#: package/contents/ui/main.qml:178
#, kde-format
msgctxt "@action:button open kwincompositing KCM"
msgid "Configure Compositing"
msgstr ""

#: package/contents/ui/main.qml:363
#, kde-format
msgctxt "@info:progress just copied a color to clipboard"
msgid "Copied!"
msgstr ""

#: package/contents/ui/main.qml:373
#, kde-format
msgctxt "@action:button"
msgid "Delete"
msgstr ""
