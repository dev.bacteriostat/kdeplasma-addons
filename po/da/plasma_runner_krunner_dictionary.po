# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Martin Schlander <mschlander@opensuse.org>, 2010, 2012, 2018.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-04 01:38+0000\n"
"PO-Revision-Date: 2018-07-12 10:34+0100\n"
"Last-Translator: Martin Schlander <mschlander@opensuse.org>\n"
"Language-Team: Danish <dansk@dansk-gruppen.dk>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: dictionaryrunner.cpp:34 dictionaryrunner_config.cpp:34
#: dictionaryrunner_config.cpp:52
#, kde-format
msgctxt "Trigger word before word to define"
msgid "define"
msgstr "definér"

#: dictionaryrunner.cpp:41
#, kde-format
msgctxt "Dictionary keyword"
msgid "%1:q:"
msgstr "%1:q:"

#: dictionaryrunner.cpp:41
#, kde-format
msgid "Finds the definition of :q:."
msgstr "Finder definitionen af :q:."

#: dictionaryrunner.cpp:118
#, kde-format
msgid "Definition for \"%1\" has been copied to clipboard"
msgstr ""

#: dictionaryrunner_config.cpp:22
#, kde-format
msgctxt "@label:textbox"
msgid "Trigger word:"
msgstr "Udløserord:"
