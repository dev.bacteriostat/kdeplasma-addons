# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Amanpreet Singh Alam <aalam@users.sf.net>, 2008.
# A S Alam <aalam@users.sf.net>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_binaryclock\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2020-09-25 02:25+0200\n"
"PO-Revision-Date: 2010-01-15 09:00+0530\n"
"Last-Translator: A S Alam <aalam@users.sf.net>\n"
"Language-Team: ਪੰਜਾਬੀ <punjabi-users@lists.sf.net>\n"
"Language: pa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: package/contents/config/config.qml:17
#, fuzzy, kde-format
#| msgid "Appearance"
msgctxt "@title"
msgid "Appearance"
msgstr "ਦਿੱਖ"

#: package/contents/ui/configGeneral.qml:35
#, kde-format
msgid "Display:"
msgstr ""

#: package/contents/ui/configGeneral.qml:36
#: package/contents/ui/configGeneral.qml:90
#, fuzzy, kde-format
#| msgid "Grid:"
msgctxt "@option:check"
msgid "Grid"
msgstr "ਗਰਿੱਡ:"

#: package/contents/ui/configGeneral.qml:41
#: package/contents/ui/configGeneral.qml:77
#, fuzzy, kde-format
#| msgid "Inactive LEDs:"
msgctxt "@option:check"
msgid "Inactive LEDs"
msgstr "ਗ਼ੈਰ-ਐਕਟਿਵ LED:"

#: package/contents/ui/configGeneral.qml:46
#, fuzzy, kde-format
#| msgid "Seconds:"
msgctxt "@option:check"
msgid "Seconds"
msgstr "ਸਕਿੰਟ:"

#: package/contents/ui/configGeneral.qml:51
#, kde-format
msgctxt "@option:check"
msgid "In BCD format (decimal)"
msgstr ""

#: package/contents/ui/configGeneral.qml:60
#, fuzzy, kde-format
#| msgid "Use custom color for active LEDs:"
msgid "Use custom color for:"
msgstr "ਐਕਟਿਵ LED ਲਈ ਪਸੰਦੀਦਾ ਰੰਗ ਵਰਤੋਂ:"

#: package/contents/ui/configGeneral.qml:64
#, fuzzy, kde-format
#| msgid "Active LEDs:"
msgctxt "@option:check"
msgid "Active LEDs"
msgstr "ਐਕਟਿਵ LED:"

#, fuzzy
#~| msgid "Show the inactive LEDs"
#~ msgctxt "@option:check"
#~ msgid "Show inactive LEDs"
#~ msgstr "ਗ਼ੈਰ-ਐਕਟਿਵ LED ਵੇਖੋ"

#, fuzzy
#~| msgid "Use custom color for active LEDs:"
#~ msgctxt "@option:check"
#~ msgid "Use custom color for active LEDs"
#~ msgstr "ਐਕਟਿਵ LED ਲਈ ਪਸੰਦੀਦਾ ਰੰਗ ਵਰਤੋਂ:"

#, fuzzy
#~| msgid "Use custom color for inactive LEDs:"
#~ msgctxt "@option:check"
#~ msgid "Use custom color for inactive LEDs"
#~ msgstr "ਗ਼ੈਰ-ਸਰਗਰਮ LED ਲਈ ਪਸੰਦੀਦਾ ਰੰਗ ਵਰਤੋਂ:"

#, fuzzy
#~| msgid "Appearance"
#~ msgctxt "@title:group"
#~ msgid "Appearance"
#~ msgstr "ਦਿੱਖ"

#~ msgid "Check this if you want to see the inactive LEDs."
#~ msgstr "ਜੇ ਤੁਸੀਂ ਬੰਦ LED ਵੇਖਣੇ ਚਾਹੁੰਦੇ ਹੋ ਤਾਂ ਇਹ ਚੁਣੋ।"

#~ msgid "Show"
#~ msgstr "ਵੇਖੋ"

#~ msgid "Use theme color"
#~ msgstr "ਥੀਮ ਰੰਗ ਵਰਤੋਂ"

#~ msgid "Show the grid"
#~ msgstr "ਗਰਿੱਡ ਵੇਖੋ"

#~ msgid "Check this if you want to see a grid around leds."
#~ msgstr "ਇਹ ਚੋਣ ਕਰੋ, ਜੇ ਤੁਸੀਂ LED ਦੇ ਦੁਆਲੇ ਗਰਿੱਡ ਵੇਖਣਾ ਚਾਹੁੰਦੇ ਹੋ।"

#~ msgid "Use custom grid color:"
#~ msgstr "ਪਸੰਦੀਦਾ ਗਰਿੱਡ ਰੰਗ ਵਰਤੋਂ:"

#~ msgid "Information"
#~ msgstr "ਜਾਣਕਾਰੀ"

#~ msgid "Show the seconds LEDs"
#~ msgstr "ਸਕਿੰਟ LED ਵੇਖੋ"

#~ msgid ""
#~ "Check this if you want to display seconds LEDs in order to see the "
#~ "seconds."
#~ msgstr "ਇਹ ਚੋਣ ਕਰੋ, ਜੇ ਤੁਸੀਂ ਸਕਿੰਟ ਵੇਖਣ ਲਈ ਸਕਿੰਟ LED ਨੂੰ ਵੇਖਣਾ ਚਾਹੁੰਦੇ ਹੋ।"

#~ msgid "General"
#~ msgstr "ਆਮ"

#~ msgid "Use custom on leds color:"
#~ msgstr "LED ਰੰਗ ਉੱਤੇ ਪਸੰਦੀਦਾ ਵਰਤੋਂ:"

#~ msgid "Show the off leds"
#~ msgstr "ਬੰਦ LED ਵੇਖੋ"

#~ msgid "Use custom off leds color:"
#~ msgstr "ਪਸੰਦੀਦਾ ਬੰਦ LED ਰੰਗ ਵਰਤੋਂ:"
